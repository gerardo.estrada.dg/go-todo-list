package main

import (
	"io"
	"net/http"
	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
)

func health(response http.ResponseWriter, request *http.Request) {
	log.Info("API Health is OK")
	response.Header().Set("Content-Type", "application/json")
	io.WriteString(response, `{"alive": true, "message": "Welcome to go"}`)
}

func init() {
	log.SetFormatter(&log.TextFormatter{})
	log.SetReportCaller(true)
}

func main() {
	log.Info("Starting Todolist API server")
	router := mux.NewRouter()
	router.HandleFunc("/health", health).Methods("GET")
	http.ListenAndServe(":8000", router)
}